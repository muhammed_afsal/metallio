import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoListingComponent } from './view/photo-listing/photo-listing.component';


const routes: Routes = [
  {path:'',component:PhotoListingComponent},
  {path:'photo-list',component:PhotoListingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
